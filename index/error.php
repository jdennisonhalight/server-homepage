<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $host; ?></title>

    <!-- Bootstrap -->
    <link href="index/css/bootstrap/bootstrap-darkly.min.css" rel="stylesheet">
    <link href="index/css/style.css" rel="stylesheet">
    <title>Oops!</title>
</head>
<body>
    <div class="container">
        <div class="jumbotron text-center">
            <h1>Oops! <br /> Something went wrong!</h1>
            <p class="bg-danger"><?php echo $error ? $error : 'Unknow error.'; ?></p>
        </div>
    </div>
</body>
</html>