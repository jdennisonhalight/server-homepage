# README #

### Quick Clone ###

1. `cd` into server folder
1.  `git init`
1. `git remote add origin git@bitbucket.org:jdennisonhalight/server-homepage.git`
1. `git pull origin master`

### Setup ###

1. Change `$mysql_host`, `$mysql_user`, and `$mysql_pass` in `index.php` for fetching MySQL version.
1. Optional: add `adminer.php` from [Adminer](http://www.adminer.org/) along side `index.php` for quick link to Adminer.

### Version ###
* 1.0.0