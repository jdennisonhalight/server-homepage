<?php

    mysqli_report( MYSQLI_REPORT_STRICT );

    // change these to connect to MySQL to fetch MySQL version
    $mysql_host = 'localhost';
    $mysql_user = 'root';
    $mysql_pass = '';

    try {
        $link = mysqli_connect( $mysql_host, $mysql_user, $mysql_pass );
        $mysql_version = mysqli_get_server_info( $link ) ? mysqli_get_server_info( $link ) : 'Unknown';
    } catch ( Exception $e ) {
        $error = $e->getMessage();
        require 'index/error.php';
        exit();
    }

    $host = gethostname() ? gethostname() : 'Unknown';

    // get all the directories in this folder
    $dirs = array_filter( glob( '*' ), 'is_dir' );

    // exclude these folder(s)
    $excludes = array(
        'index'
    );

    foreach ( $excludes as $exclude ) {
        $index = array_search( $exclude , $dirs ); // find the exclude in the dir
        if ( $index !== FALSE ) {
            unset( $dirs[ $index ] );
        }
    }

    $split1 = explode( '/', $_SERVER['SERVER_SOFTWARE'] );
    $split2 = explode( ' ', $split1[1] );

    $apache_version = $split2[0] ? $split2[0] : 'Unknown';

    $server_host = $_SERVER['HTTP_HOST'] ? $_SERVER['HTTP_HOST'] : 'Unknown';

    $php_version = phpVersion() ? phpVersion() : 'Unknown';

    $loaded_extensions = get_loaded_extensions() ? get_loaded_extensions() : 'Unknown';

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $host; ?></title>

    <!-- Bootstrap -->
    <link id="theme" href="index/css/bootstrap/bootstrap-darkly.min.css" rel="stylesheet">
    <link href="index/css/style.css" rel="stylesheet">
  </head>
  <body>
    <header>
        <div class="container">
            <div class="page-header">
                <h1><?php echo $host; ?></h1>
            </div>
            <div class="btn-group btn-group-justified" role="group">
                <?php if ( file_exists( 'adminer.php' ) ): ?>
                    <a href="/adminer.php" class="btn btn-lg btn-success" role="button"><i class="im-database"></i> Adminer</a>
                <?php endif ?>
                <?php if ( file_exists( 'phpinfo.php' ) ): ?>
                    <a href="/phpinfo.php" class="btn btn-lg btn-success" role="button"><i class="im-file-o"></i> PHP Info</a>
                <?php endif ?>
            </div>
        </div>
    </header>

    <div class="container">
        <div class="col-lg-6">
            <h2><i class="im-folder-open"></i> Directories</h2>
            <div class="list-group">
                <?php foreach ($dirs as $directory): ?>
                    <a href="/<?php echo $directory; ?>" class="list-group-item">
                        <h3 class="text-capitalize"><?php echo str_replace( array( '-', '_'), ' ', $directory ); ?></h3>
                    </a>
                <?php endforeach ?>
            </div>
        </div>

        <div class="col-lg-6">
            <h2><i class="im-info-circle"></i> Server Info</h2>

            <div class="panel panel-default">

                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td><h4>Host</h4></td>
                            <td>
                                <h4><code><?php echo $server_host; ?></code></h4>
                            </td>
                        </tr>
                        <tr>
                            <td><h4>Apache</h4></td>
                            <td>
                                <h4>
                                    <code><?php echo $apache_version; ?></code>
                                </h4>
                            </td>
                        </tr>
                        <tr>
                            <td><h4>PHP</h4></td>
                            <td>
                                <h4>
                                    <code><?php echo $php_version; ?></code>
                                </h4>
                            </td>
                        </tr>
                        <tr>
                            <td><h4>MySQL</h4></td>
                            <td>
                                <h4>
                                    <code><?php echo $mysql_version; ?></code>
                                </h4>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="im-puzzle-piece"></i> Loaded Extensions</h3>
                </div>
                <div class="panel-body">
                    <ul class="list-inline">
                        <?php foreach ($loaded_extensions as $ext): ?>
                            <li class="text-uppercase"><strong><?php echo $ext; ?></strong></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>

        </div>

    </div>

    <footer>
        <div class="container text-muted text-center">
            <p>Created by <a href="mailto:jdennison@exomark.com">Jake Dennison</a></p>
        </div>
    </footer>

  </body>
</html>